<?php
// Heading
$_['heading_title'] = 'Клієнти';

// Text
$_['text_success'] = 'Налаштування успішно змінено!';
$_['text_list'] = 'Клієнти';
$_['text_add'] = 'Додати';
$_['text_edit'] = 'Редагувати';
$_['text_default'] = 'За замовчуванням';
$_['text_account']              = 'Customer Details';
$_['text_password']             = 'Password';
$_['text_other']                = 'Other';
$_['text_affiliate']            = 'Affiliate Details';
$_['text_payment']              = 'Payment Details';
$_['text_balance'] = 'Баланс';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Bank Transfer';
$_['text_history']              = 'History';
$_['text_history_add']          = 'Add History';
$_['text_transaction']          = 'Transactions';
$_['text_transaction_add']      = 'Add Transaction';
$_['text_reward']               = 'Reward Points';
$_['text_reward_add']           = 'Add Reward Points';
$_['text_ip']                   = 'IP';
$_['text_option']               = 'Options';
$_['text_login']                = 'Login into Store';
$_['text_unlock']               = 'Unlock Account';

// Column
$_['column_name'] = 'Ім&#39;я клієнта';
$_['column_email'] = 'E-Mail';
$_['column_customer_group'] = 'Група клієнтів';
$_['column_status'] = 'Статус';
$_['column_date_added'] = 'Дата додавання';
$_['column_comment'] = 'Коментар';
$_['column_description'] = 'Опис';
$_['column_amount'] = 'Сума';
$_['column_points'] = 'Бали';
$_['column_ip'] = 'IP';
$_['column_total'] = 'Загальна сума';
$_['column_action'] = 'Дія';

// Entry
$_['entry_customer_group'] = 'Група покупців';
$_['entry_firstname'] = 'Ім&#39;я, по Батькові';
$_['entry_lastname'] = 'Прізвище';
$_['entry_email'] = 'E-Mail';
$_['entry_telephone'] = 'Телефон';
$_['entry_fax'] = 'Факс';
$_['entry_newsletter'] = 'Підписка на новини';
$_['entry_status'] = 'Статус';
$_['entry_approved'] = 'Схвалений';
$_['entry_safe'] = 'Безпечний';
$_['entry_password'] = 'Пароль';
$_['entry_confirm'] = 'Підтвердіть пароль';
$_['entry_company'] = 'Компанія';
$_['entry_address_1'] = 'Адреса 1';
$_['entry_address_2'] = 'Адреса 2';
$_['entry_city'] = 'Місто';
$_['entry_postcode'] = 'Індекс';
$_['entry_country'] = 'Країна';
$_['entry_zone'] = 'Регіон / Область';
$_['entry_default'] = 'Адреса';
$_['entry_affiliate']           = 'Affiliate';
$_['entry_tracking']            = 'Tracking Code';
$_['entry_website']             = 'Web Site';
$_['entry_commission']          = 'Commission (%)';
$_['entry_tax']                 = 'Tax ID';
$_['entry_payment']             = 'Payment Method';
$_['entry_cheque']              = 'Cheque Payee Name';
$_['entry_paypal']              = 'PayPal Email Account';
$_['entry_bank_name']           = 'Bank Name';
$_['entry_bank_branch_number']  = 'ABA/BSB number (Branch Number)';
$_['entry_bank_swift_code']     = 'SWIFT Code';
$_['entry_bank_account_name']   = 'Account Name';
$_['entry_bank_account_number'] = 'Account Number';
$_['entry_comment'] = 'Коментарі';
$_['entry_description'] = 'Опис';
$_['entry_amount'] = 'Сума';
$_['entry_points'] = 'Бали';
$_['entry_name'] = 'Ім&#39;я клієнта';
$_['entry_ip'] = 'IP';
$_['entry_date_added'] = 'Дата додавання';

// Help
$_['help_safe'] = 'Встановіть значення true, щоб схвалити цього клієнта, від того, якщо він був спійманий за системою по боротьбі з шахрайством';
$_['help_points'] = 'Використовуйте мінус для вирахування балів, наприклад -60';
$_['help_affiliate']            = 'Enable / Disable the customers ability to use the affiliate system.';
$_['help_tracking']             = 'The tracking code that will be used to track referrals.';
$_['help_commission']           = 'Percentage the affiliate receives on each order.';

// Error
$_['error_warning'] = 'Уважно перевірте форму на помилки!';
$_['error_permission'] = 'У Вас немає прав для зміни налаштувань Клієнти!';
$_['error_exists'] = 'Цей E-Mail вже зареєстрований!';
$_['error_firstname'] = 'Ім&#39;я повинно містити від 1 до 32 символів!';
$_['error_lastname'] = 'Прізвище повинна бути від 1 до 32 символів!';
$_['error_email'] = 'E-Mail адресу введено невірно!';
$_['error_telephone'] = 'Телефон повинен містити від 3 до 32 символів!';
$_['error_password'] = 'Пароль має бути від 4 до 20 символів!';
$_['error_confirm'] = 'Пароль і підтвердження паролю не співпадають!';
$_['error_address_1'] = 'Адреса має бути від 3 до 128 символів!';
$_['error_city'] = 'Місто повинен бути від 2 до 128 символів!';
$_['error_postcode'] = 'Індекс повинен бути від 2 до 10 символів!';
$_['error_country'] = 'Оберіть країну!';
$_['error_zone'] = 'Виберіть регіон!';
$_['error_custom_field']        = '%s required!';
$_['error_tracking']            = 'Tracking Code required!';
$_['error_tracking_exists']     = 'Tracking code is being used by another affiliate!';
$_['error_cheque']              = 'Cheque Payee Name required!';
$_['error_paypal']              = 'PayPal Email Address does not appear to be valid!';
$_['error_bank_account_name']   = 'Account Name required!';
$_['error_bank_account_number'] = 'Account Number required!';
