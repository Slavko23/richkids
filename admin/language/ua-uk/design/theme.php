<?php
// Heading
$_['heading_title'] = 'Редактор тим';

// Text
$_['text_success'] = 'Налаштування успішно змінено!';
$_['text_edit'] = 'Редагувати';
$_['text_store'] = 'Вибрати магазин';
$_['text_template'] = 'Вибрати шаблон';
$_['text_default'] = 'За замовчуванням';
$_['text_warning'] = 'Увага! Безпека може бути порушена з за редактора тим';
$_['text_access'] = 'Переконайтеся, що потрібні облікові записи адміністратора мають доступ до цієї сторінки, та як ви отримуєте прямий доступ до вихідного коду.';
$_['text_permission'] = 'Ви можете змінити права доступу <a href="%s" class="alert-link">тут</a>.';
$_['text_begin'] = 'Виберіть тему зліва, щоб почати редагування.';

$_['text_history']      = 'Theme History';
$_['text_twig']         = 'The theme editor uses the template language Twig. You can read about <a href="http://twig.sensiolabs.org/documentation" target="_blank" class="alert-link">Twig syntax here</a>.';

// Column
$_['column_store']      = 'Store';
$_['column_route']      = 'Route';
$_['column_theme']      = 'Theme';
$_['column_date_added'] = 'Date Added';
$_['column_action']     = 'Action';

// Error
$_['error_permission'] = 'У Вас немає прав для зміни налаштувань!';
$_['error_twig']        = 'Warning: You can only save .twig files!';