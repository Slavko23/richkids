<?php
// Heading
$_['heading_title'] = 'Переклад';

// Text
$_['text_success']     = 'Success: You have modified language editor!';
$_['text_edit'] = 'Редагувати';
$_['text_list'] = 'Переклад';
$_['text_translation'] = 'Вибрати';
$_['text_add']         = 'Add Translation';
$_['text_default']     = 'Default';
$_['text_store']       = 'Store';
$_['text_language']    = 'Language';

// Column
$_['column_store']     = 'Store';
$_['column_language']  = 'Language';
$_['column_route']     = 'Route';
$_['column_key']       = 'Key';
$_['column_value']     = 'Value';
$_['column_flag'] = 'Прапор';
$_['column_country'] = 'Країна';
$_['column_progress'] = 'Процес перекладу';
$_['column_action'] = 'Дія';

// button
$_['button_install'] = 'Встановити';
$_['button_uninstall'] = 'Видалити';
$_['button_refresh'] = 'Оновити';

// Entry
$_['entry_store']      = 'Store';
$_['entry_language']   = 'Language';
$_['entry_route']      = 'Route';
$_['entry_key']        = 'Key';
$_['entry_default']    = 'Default';
$_['entry_value']      = 'Value';

// Error
$_['error_permission'] = 'У Вас немає прав для зміни налаштувань!';
$_['error_key']        = 'Key must be between 3 and 64 characters!';