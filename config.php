<?php
// HTTP
define('HTTP_SERVER', 'http://richkids.dev/');

// HTTPS
define('HTTPS_SERVER', 'http://richkids.dev/');

// DIR
define('DIR_APPLICATION', 'S:/projects/richkids/catalog/');
define('DIR_SYSTEM', 'S:/projects/richkids/system/');
define('DIR_IMAGE', 'S:/projects/richkids/image/');
define('DIR_LANGUAGE', 'S:/projects/richkids/catalog/language/');
define('DIR_TEMPLATE', 'S:/projects/richkids/catalog/view/theme/');
define('DIR_CONFIG', 'S:/projects/richkids/system/config/');
define('DIR_CACHE', 'S:/projects/richkids/system/storage/cache/');
define('DIR_DOWNLOAD', 'S:/projects/richkids/system/storage/download/');
define('DIR_LOGS', 'S:/projects/richkids/system/storage/logs/');
define('DIR_MODIFICATION', 'S:/projects/richkids/system/storage/modification/');
define('DIR_UPLOAD', 'S:/projects/richkids/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'ilvs_richkids');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
