<?php
// Heading
$_['express_text_title'] = 'Підтвердження замовлення';

// Text
$_['text_title'] = 'PayPal Експрес-платежі (в. т. ч. карти Visa і MasterCard)';
$_['button_continue'] = 'Продовжити';
$_['text_cart'] = 'Кошик для покупок';
$_['text_shipping_updated'] = 'Доставка оновлено';
$_['text_trial'] = 'Сума: %s; Періодичність: %s %s; Кількість платежів: %s, Далі ';
$_['text_recurring'] = 'Сума: %s Періодичність: %s %s';
$_['text_recurring_item'] = 'Повторювані платежі';
$_['text_length'] = 'Кількість платежів: %s';

// Entry
$_['express_entry_coupon'] = 'Введіть код купона:';

// Button
$_['button_express_coupon']   = 'Add';
$_['button_express_confirm']  = 'Confirm';
$_['button_express_login']    = 'Continue to PayPal';
$_['button_express_shipping'] = 'Update shipping';
$_['express_button_coupon'] = 'Додати';
$_['express_button_confirm'] = 'Підтвердити';
$_['express_button_login'] = 'Увійти в PayPal';
$_['express_button_shipping'] = 'Оновити доставку';
$_['button_cancel_recurring'] = 'Cancel payments';

// Error
$_['error_heading_title'] = 'Виникла помилка ...';
$_['error_too_many_failures'] = 'В процесі оплати сталася помилка';
$_['error_unavailable'] 	  = 'Please use the full checkout with this order';
$_['error_no_shipping']    	  = 'Warning: No Shipping options are available. Please <a href="%s">contact us</a> for assistance!';